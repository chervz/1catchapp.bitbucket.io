$.getJSON("https://spreadsheets.google.com/feeds/list/17_IQGqXp3K-JnIWd2wwH6Zzq0W3viYWiJMXBIkX96L4/od6/public/values?alt=json", function (data) {
  var sheetData = data.feed.entry;
  for (i = sheetData.length - 1; i < sheetData.length; i--) {
    document.getElementById('report-data').innerHTML += (
      '<tr> \
        <td>' + sheetData[i]['gsx$subject']['$t'] + '</td> \
        <td> \
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal' + i + '">View Details</button> \
        </td> \
      </tr> \
        \
        \
        \
      <div class="modal fade" id="myModal' + i + '" role="dialog"> \
        <div class="modal-dialog"> \
          <div class="modal-content"> \
            <div class="modal-header"> \
              <h4 class="modal-title">Report Details</h4> \
            </div> \
            <div class="modal-body"> \
              <p>Subject: ' + sheetData[i]['gsx$subject']['$t'] + '</p> \
              <p>Reporter: ' + sheetData[i]['gsx$name']['$t'] + '</p> \
              <p>Phone Number: ' + sheetData[i]['gsx$phone-number']['$t'] + '</p> \
              <p>Date/Time: ' + sheetData[i]['gsx$timestamp']['$t'] + '</p> \
              <p>Description: ' + sheetData[i]['gsx$description']['$t'] + '</p> \
              <p>Uploaded Image:<br> <a href="photos/IMG_341'+ i + '.jpg" target=_blank><img src="photos/IMG_341' + i + '.jpg" height="250" width="265"></a></p>\
              <a href="#" name="'+ sheetData[i]['gsx$reporter-longitude']['$t'] +'" id="' + sheetData[i]['gsx$reporter-latitude']['$t']  + '" class="btn btn-warning btn-block reportpage" >View Location</a> \
            </div> \
            <div class="modal-footer"> \
              <button type="button" class="btn btn-info" data-dismiss="modal">Close</button> \
            </div> \
          </div> \
        </div> \
      </div>'
    );
  }
});
