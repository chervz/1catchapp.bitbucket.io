var counter = 0; 

function count() {
    $.getJSON("https://spreadsheets.google.com/feeds/list/17_IQGqXp3K-JnIWd2wwH6Zzq0W3viYWiJMXBIkX96L4/od6/public/values?alt=json", function (data) {
        var sheetData = data.feed.entry;
        var date = document.getElementById("date");
        
        for (i = 0; i < sheetData.length; ++i) {
            if (sheetData[i]['gsx$date']['$t'] == date.value) {
                counter ++;
            }
        }
        document.getElementById("count").innerHTML += counter;
    });

}

function clearData(){
    counter = 0;
}