document.getElementById("show-reporter-longitude").style.display = "none";
document.getElementById("show-reporter-latitude").style.display = "none";

$("#set-reporter-location-btn").on("click", function() {
    $(this).prop("disabled", true);
    $("#send").prop("disabled", false);
});

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    alert("Geolocation is not supported by this browser.");
  }
}

function showPosition(position) {  
  document.getElementById("show-reporter-latitude").value = position.coords.latitude;
  document.getElementById("show-reporter-longitude").value = position.coords.longitude;

  console.log(document.getElementById("show-reporter-latitude").value);
  console.log(document.getElementById("show-reporter-longitude").value);
}