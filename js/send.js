$('#gform').submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: 'https://script.google.com/a/symph.co/macros/s/AKfycbxz7V0W8ttwaU5gU9cQoZZK6JVuZ15afu62vB14kA/exec',
        type: 'post',
        data: $('#gform').serialize(),
        success: function (resp) {
            window.alert("YOUR REPORT HAS BEEN SENT");
            window.location.href = "./index.html";
        }
    });
});